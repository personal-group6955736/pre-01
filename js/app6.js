document.addEventListener("DOMContentLoaded", function () {
    const btnBuscar = document.getElementById("btnBuscar");
    const inputPais = document.getElementById("inputPais");
    const resultado = document.getElementById("resultado");
    const capitalElement = document.getElementById("capital");
    const lenguajeElement = document.getElementById("lenguaje");
    const regionElement = document.getElementById("region");
    const subregionElement = document.getElementById("subregion");


    btnBuscar.addEventListener("click", function () {
        const nombrePais = inputPais.value.trim();
        if (nombrePais !== "") {
            fetch(`https://restcountries.com/v3.1/name/${nombrePais}`)
                .then(response => response.json())
                .then(data => {
                    if (data.length > 0) {
                        const pais = data[0];
                        alert("Datos de " + nombrePais + " encontrados");
                        capitalElement.textContent = `Capital: ${pais.capital}`;
                        lenguajeElement.textContent = `Lenguaje: ${Object.values(pais.languages)[0]}`;
                        regionElement.textContent = `Region: ${pais.region}`;
                        subregionElement.textContent = `Subregion: ${pais.subregion}`;
                        resultado.style.display = "block";
                    } else {
                        resultado.style.display = "none";
                        alert("DATOS NO ENCONTRADOS");
                    }
                })
                .catch(error => {
                    console.error("Error en la petición:", error);
                    alert("ERROR DE BÚSQUEDA");
                });
        } else {
            alert("POR FAVOR INGRESE UN PAÍS");
        }
    });
});
