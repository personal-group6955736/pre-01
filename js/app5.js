document.addEventListener("DOMContentLoaded", function () {
    const btnCargar = document.getElementById("btnCargar");
    const btnLimpiar = document.getElementById("btnLimpiar");
    const inputID = document.getElementById("inputID");
    const listaUsuarios = document.getElementById("lista");
    btnCargar.addEventListener("click", function () {
        const userId = inputID.value.trim();
        if (userId !== "") {
            hacerPeticion(`https://jsonplaceholder.typicode.com/users/${userId}`);
        } else {
            alert("POR FAVOR LLENE EL CAMPO ID");
        }
    });
    btnLimpiar.addEventListener("click", function () {
        inputID.value = "";
        listaUsuarios.innerHTML = "";
    });
    function hacerPeticion(url) {
        axios.get(url)
            .then(response => {
                const vendedor = response.data;
                alert("VENDEDOR " + vendedor.id + " ENCONTRADO");
                listaUsuarios.innerHTML = `<tr>
                    <td>${vendedor.id}</td>
                    <td>${vendedor.name}</td>
                    <td>${vendedor.username}</td>
                    <td>${vendedor.email}</td>
                    <td>${vendedor.address.street}</td>
                    <td>${vendedor.address.suite}</td>
                    <td>${vendedor.address.city}</td>
                </tr>`;
            })
            .catch(error => {
                console.error("ERROR EN PETICIÓN:", error);
                alert("ID INEXISTENTE");
            });
    }
});
