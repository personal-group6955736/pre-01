const btnLista = document.getElementById("btnLista");
    const btnImagen = document.getElementById("btnImagen");
    
    btnLista.addEventListener('click', function(){
        fetchBreeds();
        btnLista.disabled = true;
    })

    btnImagen.addEventListener('click', function(){
        fetchDogImage();
    })
    
    // Function to fetch the list of dog breeds
    function fetchBreeds() {
      fetch('https://dog.ceo/api/breeds/list')
        .then(response => response.json())
        .then(data => {
          const breedsSelect = document.getElementById('breeds');
          data.message.forEach(breed => {
            const option = document.createElement('option');
            option.textContent = breed;
            breedsSelect.appendChild(option);
          });
        })
        .catch(error => console.error('Error fetching breeds:', error));
    }

    // Function to fetch a random image of the selected dog breed
    function fetchDogImage() {
      const selectedBreed = document.getElementById('breeds').value;
      fetch(`https://dog.ceo/api/breed/${selectedBreed}/images/random`)
        .then(response => response.json())
        .then(data => {
          const dogImage = document.getElementById('dog-image');
          dogImage.src = data.message;
        })
        .catch(error => console.error('Error fetching dog image:', error));
    }

    // Fetch the list of dog breeds when the page loads